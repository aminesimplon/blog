import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import co.simplon.blog.BlogApplication;

@SpringBootTest(classes = BlogApplication.class)
@AutoConfigureMockMvc
@Sql("/database.sql")
public class ArticleControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testFindAll() throws Exception {
        mvc.perform(get("/api/article"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]['titre']").exists())
                .andExpect(jsonPath("$[0]['titre']").value("Vero"));
    }

    @Test
    void findByIdSuccess() throws Exception {
        mvc.perform(get("/api/article/3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['id']").isNumber())
                .andExpect(jsonPath("$['titre']").isString())
                .andExpect(jsonPath("$['contenu']").isString())
                .andExpect(jsonPath("$['date']").exists())
                .andExpect(jsonPath("$['image']").isString())
                .andExpect(jsonPath("$['likes']").isNumber())
                .andExpect(jsonPath("$['dislikes']").isNumber());
    }

    @Test
    void findByIdNotFound() throws Exception {
        mvc.perform(get("/api/article/1000"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testPostArticle() throws Exception {
        mvc.perform(
                post("/api/article")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                    {
                                            "titre": "Nouvel article",
                                            "contenu": "Contenu de l'article 1",
                                            "auteur": "Auteur 1",
                                            "date": "2024-03-28",
                                            "image": "image1.jpg",
                                            "likes": 10,
                                            "dislikes": 2,
                                            "categorieId": 1
                                        }
                                """))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$['id']").isNumber());
    }

    @Test
    void testPostDogInvalid() throws Exception {
        mvc.perform(
                post("/api/article")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                    {
                                        "contenu": "Contenu de l'article 1",
                                        "auteur": "Auteur 1",
                                        "date": "2024-03-28",
                                        "image": "image1.jpg",
                                        "likes": 10,
                                        "dislikes": 2
                                        }
                                """))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteDog() throws Exception {
        mvc.perform(delete("/api/article/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutDog() throws Exception {
        mvc.perform(put("/api/article/3")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "titre": "Nouvel article",
                "contenu": "Contenu de l'aaaarticle 3",
                "auteur": "Auteur 33",
                "date": "2033-03-28",
                "image": "image3.jpg",
                "likes": 3,
                "dislikes": 33
            }
        """)).andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").isNumber())
        .andExpect(jsonPath("$['titre']").value("Nouvel article"))
        .andExpect(jsonPath("$['contenu']").value("Contenu de l'aaaarticle 3"))
        .andExpect(jsonPath("$['auteur']").value("Auteur 33"))
        .andExpect(jsonPath("$['date']").value("2033-03-28"))
        .andExpect(jsonPath("$['image']").value("image3.jpg"))
        .andExpect(jsonPath("$['likes']").value(3))
        .andExpect(jsonPath("$['dislikes']").value(33));
    }


}
