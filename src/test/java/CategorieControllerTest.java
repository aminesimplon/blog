import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import co.simplon.blog.BlogApplication;

@SpringBootTest(classes = BlogApplication.class)
@AutoConfigureMockMvc
@Sql("/database.sql")
public class CategorieControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void testFindAll() throws Exception {
        mvc.perform(get("/api/categorie"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id").exists())
                .andExpect(jsonPath("$[0].nom").value("Sports"));
    }

    @Test
    void findByIdSuccess() throws Exception {
        mvc.perform(get("/api/categorie/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.nom").value("Sports"));
    }

    @Test
    void findByIdNotFound() throws Exception {
        mvc.perform(get("/api/categorie/1000"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testPostCategorie() throws Exception {
        mvc.perform(post("/api/categorie")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"nom\": \"NouvelleCatégorie\" }"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.nom").value("NouvelleCatégorie"));
    }

    @Test
    void testPostCategorieInvalid() throws Exception {
        mvc.perform(post("/api/categorie")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ }"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteCategorie() throws Exception {
        mvc.perform(delete("/api/categorie/4"))
                .andExpect(status().isNoContent());
    }

    @Test
    void testPutCategorie() throws Exception {
        mvc.perform(put("/api/categorie/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"nom\": \"Sports Modifié\" }"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.nom").value("Sports Modifié"));
    }
}
