import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import co.simplon.blog.BlogApplication;

@SpringBootTest(classes = BlogApplication.class)
@AutoConfigureMockMvc
@Sql("/database.sql")
public class CommentaireControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testFindCommentairesByArticleId() throws Exception {
        mvc.perform(get("/api/commentaire/{articleId}", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0]['id']").exists())
                .andExpect(jsonPath("$[0]['contenu']").isString())
                .andExpect(jsonPath("$[0]['auteur']").isString())
                .andExpect(jsonPath("$[0]['date']").isString())
                .andExpect(jsonPath("$[0]['articleId']").exists()); // Utilisation de l'attribut 'articleId'
    }

    @Test
    void testPersistCommentaire() throws Exception {
        mvc.perform(post("/api/commentaire")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                            "contenu": "Contenu du commentaire",
                            "auteur": "Auteur du commentaire",
                            "date": "2024-03-28T10:00:00",
                            "articleId": 1
                        }
                        """))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$['id']").isNumber());
    }

    @Test
    void testDeleteCommentaire() throws Exception {
        mvc.perform(delete("/api/commentaire/{id}", 1))
                .andExpect(status().isNoContent());
    }

    @Test
    void testReplaceCommentaire() throws Exception {
        mvc.perform(put("/api/commentaire/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                            "id": 1,
                            "contenu": "Nouveau contenu du commentaire",
                            "auteur": "Nouvel auteur du commentaire",
                            "date": "2024-03-29T10:00:00",
                            "articleId": 1
                        }
                        """))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['id']").value(1))
                .andExpect(jsonPath("$['contenu']").value("Nouveau contenu du commentaire"))
                .andExpect(jsonPath("$['auteur']").value("Nouvel auteur du commentaire"))
                .andExpect(jsonPath("$['date']").value("2024-03-29T10:00:00"))
                .andExpect(jsonPath("$['articleId']").value(1)); 
    }
}
