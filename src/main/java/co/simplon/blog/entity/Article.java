package co.simplon.blog.entity;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class Article {
    public Article(String titre,
    String contenu,
            @NotBlank(message = "L'auteur ne peut pas être vide") String auteur,
            @NotNull(message = "La date ne peut pas être vide") String date, String image, int likes, int dislikes) {
        this.titre = titre;
        this.contenu = contenu;
        this.auteur = auteur;
        this.date = date;
        this.image = image;
        this.likes = likes;
        this.dislikes = dislikes;
    }

    private int id;

    @NotBlank(message = "Le titre ne peut pas être vide")
    private String titre;

    @NotBlank(message = "Le contenu ne peut pas être vide")
    private String contenu;

    @NotBlank(message = "L'auteur ne peut pas être vide")
    private String auteur;

    @NotNull(message = "La date ne peut pas être vide")
    private String date;
    private String image;
    private int likes;
    private int dislikes;
    private int categorieId;

    public Article() {
    }

    public Article(String titre, String contenu, String auteur, String date, String image, int likes, int dislikes,
            int categorieId) {
        this.titre = titre;
        this.contenu = contenu;
        this.auteur = auteur;
        this.date = date;
        this.image = image;
        this.likes = likes;
        this.dislikes = dislikes;
        this.categorieId = categorieId;
    }

    public Article(int id, String titre, String contenu, String auteur, String date, String image, int likes,
            int dislikes, int categorieId) {
        this.id = id;
        this.titre = titre;
        this.contenu = contenu;
        this.auteur = auteur;
        this.date = date;
        this.image = image;
        this.likes = likes;
        this.dislikes = dislikes;
        this.categorieId = categorieId;
    }

    public int getCategorieId() {
        return categorieId;
    }

    public void setCategorieId(int categorieId) {
        this.categorieId = categorieId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getDislikes() {
        return dislikes;
    }

    public void setDislikes(int dislikes) {
        this.dislikes = dislikes;
    }

}
