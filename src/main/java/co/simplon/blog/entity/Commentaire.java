package co.simplon.blog.entity;

public class Commentaire {
    private int id;
    private String contenu;
    private String auteur;
    private String date;
    private int articleId; // Remplace l'objet Article par l'ID de l'article

    public Commentaire(int id, String contenu, String auteur, String date, int articleId) {
        this.id = id;
        this.contenu = contenu;
        this.auteur = auteur;
        this.date = date;
        this.articleId = articleId;
    }

    public Commentaire(String contenu, String auteur, String date, int articleId) {
        this.contenu = contenu;
        this.auteur = auteur;
        this.date = date;
        this.articleId = articleId;
    }

    public Commentaire() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }
}
