package co.simplon.blog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.blog.entity.Commentaire;

@Repository
public class CommentaireRepo {

    @Autowired
    private DataSource dataSource;

    public Commentaire findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commentaire WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Commentaire(
                    result.getInt("id"),
                    result.getString("contenu"),
                    result.getString("auteur"),
                    result.getString("date"),
                    result.getInt("article_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Commentaire> findByArticleId(int articleId) {
        List<Commentaire> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM commentaire WHERE article_id = ?");
            stmt.setInt(1, articleId);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Commentaire commentaire = new Commentaire(
                    result.getInt("id"),
                    result.getString("contenu"),
                    result.getString("auteur"),
                    result.getString("date"),
                    result.getInt("article_id"));

                list.add(commentaire);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public boolean persist(Commentaire commentaire) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO commentaire(contenu, auteur, date, article_id) VALUES (?, ?, ?, ?)");
            stmt.setString(1, commentaire.getContenu());
            stmt.setString(2, commentaire.getAuteur());
            stmt.setString(3, commentaire.getDate());
            stmt.setInt(4, commentaire.getArticleId());
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM commentaire WHERE id = ?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result > 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(Commentaire commentaire) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE commentaire SET contenu = ?, auteur = ?, date = ?, article_id = ? WHERE id = ?");
            stmt.setString(1, commentaire.getContenu());
            stmt.setString(2, commentaire.getAuteur());
            stmt.setString(3, commentaire.getDate());
            stmt.setInt(4, commentaire.getArticleId());
            stmt.setInt(5, commentaire.getId());

            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
}
