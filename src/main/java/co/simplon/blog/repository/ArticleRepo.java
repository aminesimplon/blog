package co.simplon.blog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.blog.entity.Article;

@Repository
public class ArticleRepo {

    @Autowired
    private DataSource dataSource;

    public List<Article> findAll() {
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT a.*, c.nom AS categorie_nom FROM article a JOIN categorie c ON a.categorie_id = c.id");
            ResultSet result = stmt.executeQuery();
    
            while (result.next()) {
                list.add(new Article(
                    result.getInt("id"),
                    result.getString("titre"),
                    result.getString("contenu"),
                    result.getString("auteur"),
                    result.getString("date"),
                    result.getString("image"),
                    result.getInt("likes"),
                    result.getInt("dislikes"),
                    result.getInt("categorie_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Article findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article where id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Article(
                    result.getInt("id"),
                    result.getString("titre"),
                    result.getString("contenu"),
                    result.getString("auteur"),
                    result.getString("date"),
                    result.getString("image"),
                    result.getInt("likes"),
                    result.getInt("dislikes"),
                    result.getInt("categorie_id")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM article where id =?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public boolean persist(Article article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO article(titre, contenu, auteur, date, image, likes, dislikes, categorie_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            stmt.setString(1, article.getTitre());
            stmt.setString(2, article.getContenu());
            stmt.setString(3, article.getAuteur());
            stmt.setString(4, article.getDate());
            stmt.setString(5, article.getImage());
            stmt.setInt(6, article.getLikes());
            stmt.setInt(7, article.getDislikes());
            stmt.setInt(8, article.getCategorieId()); // Ajout de la catégorie
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }
    
    public boolean updateArticle(Article article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE article SET titre = ?, contenu = ?, auteur = ?, date = ?, image = ?, likes = ?, dislikes = ?, categorie_id = ? WHERE id = ?");
            stmt.setString(1, article.getTitre());
            stmt.setString(2, article.getContenu());
            stmt.setString(3, article.getAuteur());
            stmt.setString(4, article.getDate());
            stmt.setString(5, article.getImage());
            stmt.setInt(6, article.getLikes());
            stmt.setInt(7, article.getDislikes());
            stmt.setInt(8, article.getCategorieId());
            stmt.setInt(9, article.getId());
    
            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    
        return false;
    }
    

    public List<Article> findAllByCategorie(int id) {
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT a.*, c.nom AS categorie_nom FROM article a JOIN categorie c ON a.categorie_id = c.id where categorie_id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(new Article(
                    result.getInt("id"),
                    result.getString("titre"),
                    result.getString("contenu"),
                    result.getString("auteur"),
                    result.getString("date"),
                    result.getString("image"),
                    result.getInt("likes"),
                    result.getInt("dislikes"),
                    result.getInt("categorie_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
    public boolean addLike(int articleId) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE article SET likes = likes + 1 WHERE id = ?");
            stmt.setInt(1, articleId);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean addDislike(int articleId) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE article SET dislikes = dislikes + 1 WHERE id = ?");
            stmt.setInt(1, articleId);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
