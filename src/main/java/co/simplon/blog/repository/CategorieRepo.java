package co.simplon.blog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.blog.entity.Categorie;

@Repository
public class CategorieRepo {

    @Autowired
    private DataSource dataSource;

    public List<Categorie> findAll() {
        List<Categorie> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM categorie");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(new Categorie(
                        result.getInt("id"),
                        result.getString("nom")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Categorie findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM categorie WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Categorie(
                        result.getInt("id"),
                        result.getString("nom"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM categorie WHERE id = ?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result > 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean persist(Categorie categorie) {
        if (categorie.getNom() == null || categorie.getNom().isEmpty()) {
            throw new IllegalArgumentException("Le nom de la catégorie ne peut pas être vide.");
        }
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO categorie(nom) VALUES (?)");
            stmt.setString(1, categorie.getNom());
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Erreur dans le repository", e);
        }
        return false;
    }

    public boolean updateCategorie(Categorie categorie) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE categorie SET nom = ? WHERE id = ?");
            stmt.setString(1, categorie.getNom());
            stmt.setInt(2, categorie.getId());
            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
