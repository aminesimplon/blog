package co.simplon.blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.blog.entity.Article;
import co.simplon.blog.repository.ArticleRepo;
import jakarta.validation.Valid;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/article")
public class ArticleController {

    @Autowired
    private ArticleRepo repo;

    @GetMapping
    public List<Article> All() {
        if (repo == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findAll();
    }

    @GetMapping("sort/{id}")
    public List<Article> findAllByCategorie(@PathVariable int id) {
        if (repo == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findAllByCategorie(id);
    }

    @GetMapping("/{id}")
    public Article findById(@PathVariable int id) {
        if (repo.findById(id) == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean delete(@PathVariable int id) {
        findById(id);
        return repo.delete(id);

    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Article add(@Valid @RequestBody Article article) {
        repo.persist(article);
        return article;
    }

    @PutMapping("/{id}")
    public ResponseEntity<Article> replace(@PathVariable int id, @Valid @RequestBody Article article) {
        Article existingArticle = findById(id);
        article.setId(id);
        repo.updateArticle(article);
        return ResponseEntity.ok(article);
    }

    public void setRepo(ArticleRepo repo) {
        this.repo = repo;
    }

    @PutMapping("/{id}/like")
    public ResponseEntity<String> addLike(@PathVariable int id) {
        boolean result = repo.addLike(id);
        if (result) {
            return ResponseEntity.ok("Like ajouté avec succès");
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Article non trouvé");
        }
    }

    @PutMapping("/{id}/dislike")
    public ResponseEntity<String> addDislike(@PathVariable int id) {
        boolean result = repo.addDislike(id);
        if (result) {
            return ResponseEntity.ok("Dislike ajouté avec succès");
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Article non trouvé");
        }
    }
}
