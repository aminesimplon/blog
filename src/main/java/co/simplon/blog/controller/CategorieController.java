package co.simplon.blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.blog.entity.Categorie;
import co.simplon.blog.repository.CategorieRepo;
import jakarta.validation.Valid;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/categorie")
public class CategorieController {

    @Autowired
    private CategorieRepo repo;

    @GetMapping
    public List<Categorie> getAllCategories() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Categorie getCategoryById(@PathVariable int id) {
        Categorie categorie = repo.findById(id);
        if (categorie == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return categorie;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean deleteCategory(@PathVariable int id) {
        if (repo.findById(id) == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Categorie addCategory(@Valid @RequestBody Categorie categorie) {
        repo.persist(categorie);
        return categorie;
    }

    @PutMapping("/{id}")
    public ResponseEntity<Categorie> updateCategory(@PathVariable int id, @Valid @RequestBody Categorie categorie) {
        Categorie existingCategorie = repo.findById(id);
        if (existingCategorie == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        categorie.setId(id);
        repo.updateCategorie(categorie);
        return ResponseEntity.ok(categorie);
    }

    public void setRepo(CategorieRepo repo) {
        this.repo = repo;
    }
}
