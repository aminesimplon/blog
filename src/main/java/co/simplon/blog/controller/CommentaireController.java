package co.simplon.blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
    
import co.simplon.blog.entity.Commentaire;
import co.simplon.blog.repository.CommentaireRepo;
import jakarta.validation.Valid;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/commentaire")
public class CommentaireController {

    @Autowired
    private CommentaireRepo repo;

    @GetMapping("/{articleId}")
    public List<Commentaire> findByArticleId(@PathVariable int articleId) {
        return repo.findByArticleId(articleId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Commentaire persist(@Valid @RequestBody Commentaire commentaire) {
        repo.persist(commentaire);
        System.out.println(commentaire.getContenu() + " " + commentaire.getArticleId());
        return commentaire;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean delete(@PathVariable int id) {
        findById(id);
        return repo.delete(id);
    }

    @PutMapping
    public Commentaire replace(@Valid @RequestBody Commentaire commentaire) {
        findById(commentaire.getId());
        commentaire.setId(commentaire.getId());
        repo.update(commentaire);
        return commentaire;
    }

    @GetMapping("/id/{id}")
    private Commentaire findById(@PathVariable int id) {
        return repo.findById(id);

    }
}
