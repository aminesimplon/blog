-- Active: 1709545637393@@127.0.0.1@3306@blog

DROP TABLE IF EXISTS commentaire;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS categorie;
DROP TABLE IF EXISTS user;



CREATE TABLE categorie (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR (64) 
);

INSERT INTO categorie (nom) VALUES
('HIP HOP'),
('Rock'),
('Rap'),
('Drill');


CREATE TABLE article (
    id INT PRIMARY KEY AUTO_INCREMENT,
    titre VARCHAR (64) NOT NULL,
    contenu VARCHAR (1500) NOT NULL,
    auteur VARCHAR (64) NOT NULL,
    date VARCHAR (255) NOT NULL,
    image VARCHAR (255) NOT NULL,
    likes INT NOT NULL,
    dislikes INT NOT NULL,
    categorie_id INT,
    FOREIGN KEY (categorie_id) REFERENCES categorie(id)
    );

CREATE TABLE commentaire (
    id INT PRIMARY KEY AUTO_INCREMENT,
    contenu VARCHAR (224) NOT NULL,
    auteur VARCHAR (64) NOT NULL,
    date VARCHAR(64) NOT NULL,
    article_id INT NOT NULL,
    FOREIGN KEY (article_id) REFERENCES article(id) ON DELETE CASCADE
);



CREATE TABLE user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    pseudo VARCHAR(64) NOT NULL UNIQUE,
    role VARCHAR(64) NOT NULL
);
INSERT INTO user (email,password,pseudo ,role) VALUES 
('admin@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq','Amine', 'ROLE_ADMIN'),
('test@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq','admin', 'ROLE_USER');
